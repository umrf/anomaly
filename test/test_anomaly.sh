# DADA2
Rscript ../dada2/dada2_process.R -a 16S -p ./reads -q TRUE -c TRUE

# TAXONOMY
DB1=./bank/SILVA_SSU_r132_March2018.RData
DB2=./bank/DAIRYdb_v1.2.0_20190222_IDTAXA.RData
Rscript ../taxonomy/assign_taxo.R -r dada2_out/robjects.Rdata \
  -o tax_out -i ${DB1},${DB2}

# TREE
Rscript ../tools/generate_tree.R -r dada2_out/robjects.Rdata -o tree_out/

# PHYLOSEQ OBJECT
Rscript ../tools/generate_phyloseq.R -r tree_out/robjects.Rdata \
  -t dada2_out/robjects.Rdata -q tax_out/robjects.Rdata \
  -m sample-metadata.csv

# DECONTAM
Rscript ../filtering/decontam.r -r phyloseq/robjects.Rdata \
 -o decontam_out/ -c type -i control -s sample \
 -m prevalence -t 0.5 -p FALSE -n 0

# PLOT COMPO
Rscript ../plot_stats/bars.R -r decontam_out/robjects.Rdata \
 -a temps -b lot -i Phylum,Genus -e lot

# DIV alpha
Rscript ../plot_stats/diversity.R -r decontam_out/robjects.Rdata \
 -a temps -b lot -m Observed,Shannon,Simpson

# DIV beta
Rscript ../plot_stats/diversity_beta.R -r decontam_out/robjects.Rdata \
 -a temps -b lot

# Differential analysis
## METACODER
 Rscript ../plot_stats/metacoder.r -r decontam_out/robjects.Rdata \
   -a temps_lot -i Genus -m 10 -c T6_lot1~T6_lot3,T9_lot1~T9_lot3

## DESEQ2
 Rscript ../plot_stats/DESeq2.R -r decontam_out/robjects.Rdata \
   -a temps_lot -i Genus -o ./deseq/ -c T6_lot1~T6_lot3,T9_lot1~T9_lot3

## METAGENOMESEQ
 Rscript ../plot_stats/metagenomeSeq.R -r ./decontam_out/robjects.Rdata \
   -a temps_lot -i Genus -o ./metagenomeSeq/ -c T6_lot1~T6_lot3,T9_lot1~T9_lot3

## AGGREGATE
 Rscript ../plot_stats/aggregate_diff.R -r ./decontam_out/robjects.Rdata \
   -m ./metacoder/metacoder_temps_lot_Genus.csv \
   -d ./deseq/ -g ./metagenomeSeq/ \
   -a temps_lot -i Genus -o ./aggregate_diff/ -c T6_lot1~T6_lot3,T9_lot1~T9_lot3
