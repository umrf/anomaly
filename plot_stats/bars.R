library(futile.logger)
suppressMessages(library("optparse"))
options(warn=-1)

option_list = list(
  make_option(c("-r", "--rdata"), type="character", default=NULL,
              help="Rdata file path.", metavar="path"),
  make_option(c("-t", "--bar"), type="logical", default=TRUE,
              help="Plotting bar plot with raw reads number. [default= %default]", metavar="character"),
  make_option(c("-c", "--compo"), type="logical", default=TRUE,
              help="Plotting with relative composition. [default= %default]", metavar="character"),
	make_option(c("-o", "--out"), type="character", default="./plot_bar/",
              help="output .Rdata file name for plot_bar. [default= %default]", metavar="path"),
  make_option(c("-a", "--column1"), type="character", default="",
              help="Column name used to sort samples on barplot. [default= %default]", metavar="STR"),
  make_option(c("-b", "--column2"), type="character", default="",
              help="Column name used to split barplot. [default= %default]", metavar="STR"),
  make_option(c("-s", "--sname"), type="logical", default=FALSE,
              help="Change sample.id by they factor in graph. [default= %default]", metavar="STR"),
  make_option(c("-n", "--num"), type="integer", default=10,
              help="Number of top taxon to display. [default= %default]", metavar="STR"),
  make_option(c("-e", "--rare"), type="character", default=NULL,
              help="Column name for rare curves. [default= %default]", metavar="STR"),
  make_option(c("-i", "--rank"), type="character", default="Genus",
              help="Taxonomic rank name. You can provide multiple ranks seperate by comma.[default= %default]", metavar="STR")
);
opt_parser = OptionParser(option_list=option_list)
opt = parse_args(opt_parser)

thisFile <- function() {
        cmdArgs <- commandArgs(trailingOnly = FALSE)
        needle <- "--file="
        match <- grep(needle, cmdArgs)
        if (length(match) > 0) {
                # Rscript
                return(normalizePath(sub(needle, "", cmdArgs[match])))
        } else {
                # 'source'd via R console
                return(normalizePath(sys.frames()[[1]]$ofile))
        }
}
PATHanomaly = gsub("plot_stats/bars.R", "", thisFile())

if (is.null(opt$rdata)){
  print_help(opt_parser)
  stop("You must provide the Rdata file path.", call.=FALSE)
}

if(opt$compo==FALSE & opt$bar==FALSE){
  stop("No plot type selected.", call.=FALSE)
}

flog.info('Creating output directory...')
if(!dir.exists(opt$out)){
  opt$out <- paste(getwd(),'/',opt$out,'/',sep='')
  dir.create(opt$out, recursive=TRUE)
}else{flog.info('Output directory already exists, please rename/delete it...'); quit()}
flog.info('Done.')

flog.info('Loading libraries...')
suppressMessages(library('phyloseq'))
suppressMessages(library('ggplot2'))
suppressMessages(library('gridExtra'))
suppressMessages(library('grid'))
suppressMessages(library('microbiome'))
suppressMessages(library('viridis'))
# suppressMessages(source("https://raw.githubusercontent.com/mahendra-mariadassou/phyloseq-extended/master/load-extra-functions.R"))
suppressMessages(library(phyloseq.extended))
suppressMessages(library(plotly))
load(opt$rdata)
flog.info('Done.')
taxonomy <- sapply(strsplit(opt$rank,","), '[')


#Gestion NA
fun <- paste("data <- subset_samples(data, !is.na(",opt$column1,"))",sep="")
eval(parse(text=fun))

# Bars
rmd_data=list()
if(opt$bar==TRUE){

  for(i in 1:length(taxonomy)){
		j <- taxonomy[i]; print(j)

    psobj.top <- microbiome::aggregate_top_taxa(data, j, top = opt$num)
    tn = taxa_names(psobj.top)
    tn[tn=="Other"] = tn[length(tn)]
    tn[length(tn)] = "Other"
    if(opt$column1 != '' & opt$column2 == ''){
      flog.info('Plotting bar (%s)...',j)
      if(opt$sname == TRUE){
        plot.composition.COuntAbun <- microbiome::plot_composition(psobj.top, x.label = opt$column1, sample.sort=opt$column1, otu.sort=tn)
      } else{
        plot.composition.COuntAbun <- microbiome::plot_composition(psobj.top, x.label = "sample.id", sample.sort=opt$column1, otu.sort=tn)
      }

      plot.composition.COuntAbun <- plot.composition.COuntAbun + theme(legend.position = "bottom") +
        theme_bw() + scale_fill_viridis(discrete = TRUE, direction=-1) +
        theme(axis.text.x = element_text(angle = 90)) +
        ggtitle("Raw abundance") + theme(legend.title = element_text(size = 18))


      flog.info('Done.')
    }
    else if(opt$column1 != '' & opt$column2 != ''){
      flog.info('Plotting bar (%s)...',j)

      sdata = psobj.top@sam_data@.Data
      names(sdata) = psobj.top@sam_data@names
      FACT1=sdata[[opt$column2]]
      (lvls = levels(as.factor(sdata[[opt$column2]])))
      LL <- list()
      for(i in 1:length(lvls)){
        fun  <- paste("ppp <- subset_samples(psobj.top, ",opt$column2," %in% '",lvls[i],"')",sep="")
        eval(parse(text=fun))
        tn = taxa_names(ppp)
        tn[tn=="Other"] = tn[length(tn)]
        tn[length(tn)] = "Other"
        if(opt$sname){
          p1 <- microbiome::plot_composition(ppp, x.label = opt$column1, verbose=TRUE,sample.sort=opt$column1, otu.sort=tn)
        } else{
          p1 <- microbiome::plot_composition(ppp, x.label = "sample.id", verbose=TRUE,sample.sort=opt$column1, otu.sort=tn)
        }


        p1 <- p1 + theme(legend.position = "bottom") +
        theme_bw() + scale_fill_viridis(discrete = TRUE, direction=-1) +
        theme(axis.text.x = element_text(angle = 90), legend.title = element_text(size = 18)) +
        ggtitle(paste("Raw abundance",opt$column2,"=", lvls[i]))

        ###ici legende separee
        if(i < length(lvls)){
          p2 <- p1 + theme(legend.position = "none")
          LL[[i]] <- p2
        }else{
          p2 <- p1 + theme(legend.position = "none")
          LL[[i]] <- p2
          leg = ggpubr::get_legend(p1)
          LL[[i+1]] <- ggpubr::as_ggplot(leg)
        }
      }

      plot.composition.COuntAbun <- p3 <- gridExtra::grid.arrange(grobs=LL, ncol=length(lvls)+1)
      flog.info('Done.')
    }

    fun <- paste('rmd_data$bars$',j,' <- plot.composition.COuntAbun',sep='')
    eval(parse(text=fun))
  }
}




# Composition
compo <- function (rankList, psobj, var = 'sample.id', col2='') {
  if(col2 != ''){
    sdata = psobj@sam_data@.Data
    names(sdata) = psobj@sam_data@names
    FACT1=sdata[[col2]]
    lvls = levels(as.factor(sdata[[col2]]))

    for(i in 1:length(rankList)){
      j <- rankList[i]
      LL <- list()
      for(k in 1:length(lvls)){
        LVL=lvls[k]
        fun  <- paste("ppp <- subset_samples(psobj, ",opt$column2," %in% '",LVL,"')",sep="")
        eval(parse(text=fun))
        flog.info('Plotting %s composition (%s)...',var, j)
        psobj.fam <- microbiome::aggregate_top_taxa(ppp, j, top = opt$num)
        psobj.rel <-  microbiome::transform(psobj.fam, "compositional")
        tn = taxa_names(psobj.rel)
        tn[tn=="Other"] = tn[length(tn)]
        tn[length(tn)] = "Other"
        p1 <- microbiome::plot_composition(psobj.rel, x.label = opt$column1, sample.sort=opt$column1, otu.sort=tn) +
        theme() + theme_bw() + scale_fill_viridis(discrete = TRUE, direction=-1) +
        theme(axis.text.x = element_text(angle = 90), legend.title = element_text(size = 18)) +
        ggtitle(paste("Relative abundance",opt$column2,"=", LVL))

        if(k < length(lvls)){
          p2 <- p1 + theme(legend.position = "none")
          LL[[k]] <- p2
        }else{
          p2 <- p1 + theme(legend.position = "none")
          LL[[k]] <- p2
          leg = ggpubr::get_legend(p1)
          LL[[k+1]] <- ggpubr::as_ggplot(leg) + theme( plot.background = element_blank())
        }

      }
      p3 <- gridExtra::grid.arrange(grobs=LL, ncol=length(lvls)+1)
      flog.info('Done.')

      # fun <- paste('rmd_data$compo$',j,'[["',opt$column2,'_',LVL,'"]]',' <- p3',sep='')
      fun <- paste('rmd_data$compo$',j,' <- p3',sep='')
      eval(parse(text= fun))
      flog.info('Done.')
    }


  }else{

    for(i in 1:length(rankList)){
      j <- rankList[i]
      flog.info('Plotting %s composition (%s)...',var, j)
      psobj.fam <- microbiome::aggregate_top_taxa(psobj, j, top = opt$num)
      psobj.rel <-  microbiome::transform(psobj.fam, "compositional")
      tn = taxa_names(psobj.rel)
      tn[tn=="Other"] = tn[length(tn)]
      tn[length(tn)] = "Other"

      p1 <- microbiome::plot_composition(psobj.rel, x.label = opt$column1, sample.sort=opt$column1, otu.sort=tn) +
      theme() + theme_bw() + scale_fill_viridis(discrete = TRUE, direction=-1) +
      theme(axis.text.x = element_text(angle = 90), legend.title = element_text(size = 18)) +
      ggtitle("Relative abundance")
      fun <- paste('rmd_data$compo$',j,' <- p1',sep='')
      eval(parse(text= fun))
      flog.info('Done.')
    }
  }

  return(rmd_data)
}
#

if(!is.null(opt$rare)){
  flog.info('Plotting rarefaction ...')
  GROUPE=opt$rare
  plot_rare <- ggrare(data, step = 100, color = opt$rare, plot = FALSE)
  plot_rare <- plot_rare + facet_wrap(GROUPE, ncol = 4) + theme_bw()
  rmd_data$rare <- ggplotly(plot_rare)
  flog.info('Done.')
}

#Coupage selon le facteur 2
if(opt$compo==TRUE){
  if(opt$column1 != '' & opt$column2 != ''){
    vector <- levels(data.frame(sample_data(data)[,opt$column2])[,1])
      rmd_data <- compo(taxonomy,data,col2=opt$column2)

  }else{
    rmd_data <- compo(taxonomy,data,opt$column1)
  }
}


# print(rmd_data)
# quit()

# Generating rmd template for report
# PATHanomaly="/home/erifa/Repository/LRF/anomaly/"
sink(paste(PATHanomaly,'/markdown/bars2.Rmd', sep=""))
cat("---
title: Bar plot
fig_width: 24
params:
  rmd_data: p
  col1: col1
---

```{r message=FALSE, warning=FALSE, include=FALSE, results='hide'}
rmd_data <- params$rmd_data

```

```{r hold=TRUE, echo=FALSE, comment = FALSE, message= FALSE, warning = FALSE, results='asis',fig.keep='all', fig.align='left', fig.width = 10, fig.height = 10}
if('rare' %in% names(rmd_data)){
  cat('# Rarefaction plot\\n')
  rmd_data$rare
}
```

```{r hold=TRUE, echo=FALSE, comment = FALSE, message= FALSE, warning = FALSE, results='asis',fig.keep='all', fig.align='left', fig.width = 20, fig.height = 10}
if('bars' %in% names(rmd_data)){
  cat('# Plot raw value composition')
  cat('\\n')
}
```

")

for(Nplot in names(rmd_data$bars)){
  if(Nplot %in% names(rmd_data$bars)){
cat(paste("
```{r hold=TRUE, echo=FALSE, comment = FALSE, message= FALSE, warning = FALSE, results='asis',  fig.keep='all', fig.align='left', fig.width = 20, fig.height = 10}
    cat('\\n')
    cat('## ",Nplot,"')
    cat('\\n')
    grid.draw(rmd_data$bars[['",Nplot,"']])
```
  ", sep=""))
  }

}


cat("
```{r hold=TRUE, echo=FALSE, comment = FALSE, message= FALSE, warning = FALSE, results='asis',fig.keep='all', fig.align='left', fig.width = 20, fig.height = 10}
if('compo' %in% names(rmd_data)){
  cat('# Composition plot')
  cat('\\n')
}
```
")

for(Nplot in names(rmd_data$compo)){
  if(Nplot %in% names(rmd_data$compo)){
cat(paste("
```{r hold=TRUE, echo=FALSE, comment = FALSE, message= FALSE, warning = FALSE, results='asis',  fig.keep='all', fig.align='left', fig.width = 20, fig.height = 10}
    cat('\\n')
    cat('## ",Nplot,"')
    cat('\\n')
    grid.draw(rmd_data$compo[['",Nplot,"']])
```
  ", sep=""))
  }

}

sink()
# cat(paste('## ',",Nplot,",sep=''))

rmarkdown::render(paste(PATHanomaly,'/markdown/bars2.Rmd', sep=""),params= list('rmd_data' = rmd_data, 'col1' = opt$column1),output_file=paste(opt$out,'/','bars.html',sep=''))  ## determiner automatiquement le path du md
flog.info('Finish.')
